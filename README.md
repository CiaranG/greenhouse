
Requirements
------------

Packages (Debian-based):
* libbluetooth-dev

Pip:

* PyBluez
* influxdb
* serial\_device

Warning
-------

This is all probably completely useless at this stage, unless you are me. It's
half-finished, unnecessarily confusing in parts, and while it works for me it
might not work for you. You have been warned.

Bluetooth Setup
---------------

I'm using a JY-MCU bluetooth module, connected to pins 0 and 1 (TX/RX).

Before starting, the device needs pairing:

 hcitool scan

That should get you the address of the device. It's called 'linvor' by
default, and the address is shown alongside in the form nn:nn:nn:nn:nn:nn
which you should substitute into the following:

 sudo bluez-simple-agent hci0 nn:nn:nn:nn:nn:nn

Then enter the PIN (default 1234) when requested.

Serial Ports
------------

Just find the appropriate /dev/TTYxxxx device.

Make sure the user running this has permission to read it. (dialout group!)

Config
------

Create a config.json file something like the following:

```
{

    "devices": [
        {
            "devid": 2,
            "location": "Greenhouse",
            "hts1": {
                "type": "dht11",
                "pin": 3,
                "name": "Greenhouse"
            },
            "send": "serial"
        }
    ],

    "sensors": [
        {
            "enabled": true,
            "devid": 2,
            "sensor": "HTS1",
            "id": 0,
            "name": "Greenhouse",
            "host": "fewston",
            "connection": "serial",
            "address": "/dev/ttyACM0"
        }
    ],

    "influxenabled": true,
    "influxaddress": "https://myinstance/subdir",
    "influxdb": "main",
    "influxuser": "myname",
    "influxpassword": "1234",

    "telegrafenabled": true
}
```

where:
 
 in devices...
   devid is a numeric device ID. There can be any number of devices in
         the 'network'. Not only that, but devices can comminicate with each
         other, such that device 1 sends its readings to device 2 over RF, and
         then device 2 reports those over serial to a PC (for example).
   hts1 defines a temperature/humidity sensor
   send defines how the device sends its results. When a device is going to be
         directly connected via USB, or sending over bluetooth, you set this
	 to "serial". When it's using RF sending, you set it to "rf".
	 (the idea with RF mode is that another device with an RF chip receives
	 the readings, and repeats them over serial/bluetooth to get them to
	 their ultimate destination - I'm not even sure if I've implemented that
	 part yet - did you read the warning at the top?)

 in sensors...
   devid is the sensor that the device is ATTACHED to (it may ultimately report
         via a different device)
   sensor is the name of the hardware sensor being used. HTS1 will refer to a
         temperature/humidity sensor defined under 'hts1' in a device.
   id is the sensor ID this will ultimately get reported as in the database
   host is the host this sensor can be read from
   connection (serial/bluetooth) is the connection, from that host
   address is the address from that host (/dev/TTYxxx for serial, or a bluetooth
       address)

 other stuff...
   influxXXX are the InfluxDB connection parameters
   telegrafXXX are the Telegraf connection parameters (currently hard-wired to
   	port 8094 on localhost as a UDP line format receiver)

Programming
-----------

Obviously before you can connect up the devices you need to program them. As
the config contains everything we need to know, you simply need to hook up
the relevant arduino device via USB, and run program.py with -d X where X is
the device ID you're programming.

Once that's done, the device will be set up to read the sensors it's supposed
to have and report the results over the communications link it's configured to
use.

Running
-------

Running sample.py will grab samples from all configured and enabled remote
monitors that are attached to the machine you run it on, and push them to
any configured destinations.

