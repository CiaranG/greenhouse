#!/usr/bin/env python2
from setuptools import setup

setup(name='greenhouse',
      version='0.0.1',
      description='Greenhouse',
      long_description=open('README.md').read(),
      author='Ciaran Gultnieks',
      author_email='ciaran@ciarang.com',
      url='http://ciarang.com',
      packages=['greenhousecore'],
      scripts=['greenhouse'],
      install_requires=[
          'PyBluez',
          'influxdb',
          'serial_device'
      ],
      extras_require={
          'dev': [
              'pyflakes',
              'flake8',
              'pytest'
              ]
          },
      classifiers=[
          'Development Status :: 3 - Alpha',
          'Intended Audience :: Developers',
          'License :: OSI Approved :: MIT License'
          'Operating System :: POSIX',
          'Topic :: Internet :: WWW/HTTP :: HTTP Servers',
      ]
      )
