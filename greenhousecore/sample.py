#!/usr/bin/python

from __future__ import division
from __future__ import unicode_literals
from __future__ import absolute_import
from __future__ import print_function

import serial
import datetime
import sys
import json
import socket
import re
from optparse import OptionParser
from influxdb import InfluxDBClient


influxclient = None


def write_influx(measurement, sensorname, value, cfg):
    """
    Write a value to InfluxDB.

    TODO: if we can't contact the server, we should locally buffer the
          readings and send later.

    :param measurement: The type of measurement, e.g. "temperature"
    """
    global influxclient
    json_body = [
        {
            "measurement": measurement,
            "tags": {
                "sensor": sensorname
            },
            "time": datetime.datetime.utcnow().isoformat(),
            "fields": {
                "value": value
            }
        }
    ]

    # TODO: giving it dummy host/port and then overwriting the innards
    #       as the API doesn't allow the influxdb endpoint not
    #       being in the root of the 'site'
    if influxclient is None:
        influxclient = InfluxDBClient('localhost', 1, cfg['influxuser'], cfg['influxpassword'], cfg['influxdb'], timeout=10)
        influxclient._InfluxDBClient__baseurl = cfg['influxaddress']
        influxclient._scheme = cfg['influxaddress'].split(':')[0]

    influxclient.write_points(json_body)


def socketlines(socket):
    buffer = socket.recv(1024)
    done = False
    while not done:
        if "\n" in buffer:
            (line, buffer) = buffer.split("\n", 1)
            yield line
        else:
            more = socket.recv(1024)
            if not more:
                done = True
            else:
                buffer += more
    if buffer:
        yield buffer


# Parse a line of input like this...
#   D:2 HTS1: H:66.00 T:6.00
def parse_htinput(line):
    print("Parsing: " + line)
    m = re.match('D:(\d+) ([0-9A-Z]+): H:([0-9.]+) T:([0-9.]+)', line)
    if m is None:
        print("...no match")
        return None
    print("Found " + m.group(3) + "/" + m.group(4) + " on " + m.group(1) + " " + m.group(2))
    return (int(m.group(1)), m.group(2), m.group(3), m.group(4))


# Attempt to read current values from the given connection/address.
# Returns (remperature, humidity) - if something does wrong then one
# or both might be None.
#  'devid' - the numeric device ID
#  'sensor' - the numeric sensor ID
def get_current(connection, address, devid, sensor):

    if connection != "bluetooth" and connection != "serial":
        print("Unsupported connection type " + connection)
        return False

    temperature = None
    humidity = None
    sock = None
    ser = None

    try:

        print("Attempting " + connection + " connection to " + address)
        if connection == 'bluetooth':
            import bluetooth
            sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
            sock.connect((address, 1))
            print("Connected...")
            for line in socketlines(sock):
                line = line.strip()
                res = parse_htinput(line)
                if res is not None:
                    td, ts, th, tt = res
                    if td == devid and ts == sensor:
                        humidity = th
                        temperature = tt
                    else:
                        print("...wrong device/sensor")
                if not options.monitor and humidity and temperature:
                    break
        else:
            ser = serial.Serial(port=address, baudrate=9600)
            print("Connected...")
            for line in ser:
                line = line.strip()
                if options.verbose:
                    print("INPUT:" + line)
                res = parse_htinput(line)
                if res is not None:
                    td, ts, th, tt = res
                    if td == devid and ts == sensor:
                        humidity = th
                        temperature = tt
                    else:
                        print("...wrong device/sensor")
                if not options.monitor and humidity and temperature:
                    break

    except Exception as e:
        print(e)
    finally:
        if sock:
            sock.close()
        if ser:
            ser.close()

    return (temperature, humidity)


# Load config...
cfgf = open('config.json', 'r')
cfg = json.load(cfgf)
cfgf.close()


# Parse command line...
parser = OptionParser()
parser.add_option(
    '-t', '--testsensor', default=None,
    help='Just test the given sensor, as defined in the config'
)
parser.add_option(
    '-m', '--monitor', default=False, action="store_true",
    help='With --testsensor, monitor continuously'
)
parser.add_option(
    '-v', '--verbose', default=False, action="store_true",
    help='Verbose output'
)
(options, args) = parser.parse_args()


if options.monitor and not options.testsensor:
    print("You can only use --monitor with --testsensor")
    sys.exit(1)

thishost = socket.gethostname()

# Decide which sensors we're going to look at...
sensors = []
for sensor in cfg['sensors']:
    if options.testsensor:
        if sensor['name'] == options.testsensor:
            if sensor['host'] != thishost:
                print("Wrong host - that sensor must be read from " + sensor['host'])
                sys.exit(1)
            sensors.append(sensor)
    else:
        if sensor['enabled'] and sensor['host'] == thishost:
            sensors.append(sensor)

# Look at them...
for sensor in sensors:

    attempts = 0
    while True:
        temperature, humidity = get_current(sensor['connection'], sensor['address'], sensor['devid'], sensor['sensor'])
        if temperature and humidity:
            print("Read values - temperature=" + temperature + " humidity=" + humidity)

            if cfg.get('influxenabled', False) and not options.testsensor:
                write_influx('temperature', sensor['name'], int(float(temperature)), cfg)
                write_influx('humidity', sensor['name'], int(float(humidity)), cfg)
                print("Values written to influxdb")

            if cfg.get('telegrafenabled', False) and not options.testsensor:
                data = unicode('temperature,main=yes,sensor={0} value={1}\nhumidity,main=yes,sensor={0} value={2}i\n'.format(
			sensor['name'].replace(' ', r'\ '), float(temperature), int(float(humidity))))
                sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
                sock.sendto(data.encode('utf8') + b'\n', ('localhost', 8094))
                sock.close()
                print("Values written to telegraf: {}".format(data))
            break
        else:
            print("...no values")

        attempts += 1
        if attempts > 10:
            print("Failed")
            break

print("Finished")
sys.exit(0)
