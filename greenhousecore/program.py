#!/usr/bin/python

import sys
import json
import subprocess
from optparse import OptionParser


# Load config...
cfgf = open('config.json', 'r')
cfg = json.load(cfgf)
cfgf.close()


# Parse command line...
parser = OptionParser()
parser.add_option(
    '-d', '--devid', default=None,
    help='Specify the device ID to program'
)
parser.add_option(
    '-p', '--port', default=None,
    help="Specify port for programming, e.g. '/dev/ttyACM0'"
)
(options, args) = parser.parse_args()

if not options.devid:
    print "Device ID required"
    sys.exit(1)
if not options.port:
    print "Port required"
    sys.exit(1)

targetdev = None
for dev in cfg['devices']:
    if dev['devid'] == int(options.devid):
        targetdev = dev
        break
if not targetdev:
    print "No such device"
    sys.exit(1)

defs = ['DEVID=' + str(targetdev['devid'])]
if 'hts1' in targetdev:
    if targetdev['hts1']['type'] == 'dht11':
        stype = '1'
    elif targetdev['hts1']['type'] == 'dht22':
        stype = '2'
    else:
        print "Invalid hts1 type"
        sys.exit(1)
    defs += ['HTS1=' + stype, 'HTS1PIN=' + str(targetdev['hts1']['pin'])]
else:
    defs += ['HTS1=0']
if 'hts2' in targetdev:
    if targetdev['hts2']['type'] == 'dht11':
        stype = '1'
    elif targetdev['hts2']['type'] == 'dht22':
        stype = '2'
    else:
        print "Invalid hts2 type"
        sys.exit(1)
    defs += ['HTS2=' + stype, 'HTS2PIN=' + str(targetdev['hts2']['pin'])]
else:
    defs += ['HTS2=0']
if targetdev['send'] == 'serial':
    defs += ['SEND=0']
elif targetdev['send'] == 'rf':
    defs += ['SEND=1']
else:
    print "Invalid send type"
    sys.exit(1)

if 'receive' in targetdev:
    if targetdev['receive'] != "rf":
        print "Invalid receive type"
        sys.exit(1)
    defs += ['RECEIVER=1']
else:
    defs += ['RECEIVER=0']

cxxflags = ""
for defn in defs:
    if len(cxxflags) != 0:
        cxxflags += " "
    cxxflags += "-D" + defn

print "Building with: " + cxxflags

if subprocess.call('ARDMK_DIR=/usr/share/arduino make CXXFLAGS="{0}" USER_LIB_PATH=libs MONITOR_PORT={1} clean upload'
        .format(cxxflags, options.port),
        shell=True, cwd='arduino') != 0:
    print "Failed"
    sys.exit(1)

print "Finished"
sys.exit(0)
