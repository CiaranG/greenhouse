import DHT22
import pyb

while True:

    try:

        pyb.delay(3000)
        out = pyb.USB_VCP()
        out.setinterrupt(-1)
        out.send("Ciaran's pyboard\n")

        led = pyb.LED(1)
        DHT22.init(timer_id = 2, nc_pin = 'Y7', gnd_pin = 'Y8', vcc_pin = 'Y5', data_pin = 'Y6')
        while True:
            try:
                h, t = DHT22.measure()
                out.send("D:3 HTS3: H:{0} T:{1}\n".format(h, t))
            except Exception as e:
                out.send("D:3 HTS3: E:" + e.message + "\n")
            led.toggle()
            pyb.delay(3000)

    except:
        pass
