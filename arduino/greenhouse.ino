
#include <avr/sleep.h>
#include <avr/power.h>
#include <avr/wdt.h>
#include <stdio.h>

#include "dht.h"
#include "RF24.h"

dht DHT;

#if RECEIVER == 1
RF24 radio(9, 10);
#endif

static FILE uartout = {0};
static int uart_putchar(char c, FILE *stream)
{
	Serial.write(c);
	return 0;
}
static void uart_flush()
{
	loop_until_bit_is_set(UCSR0A, TXC0);
}

void setup()
{

 	// By default, all pins (leaving TX/RX alone) set to inputs with
 	// pullups enabled, to ensure no unwanted power usage.
 	DDRD &= B00000011;
 	DDRB = B00000000;
 	PORTD |= B11111100;
 	PORTB |= B11111111;

 	// Not using the ADC, so disable to save power.
 	ADCSRA = 0;
 	PRR = 1;

 	power_twi_disable();
#if RECEIVER == 0
 	power_spi_disable();
#endif

	// Set up serial communications, with stdout also redirected there...
	Serial.begin(9600);
	fdev_setup_stream(&uartout, uart_putchar, NULL, _FDEV_SETUP_WRITE);
	stdout = &uartout;

 	Serial.println("Ciaran's Greenhouse 2");

#if RECEIVER == 1
	radio.begin();
	radio.startListening();
	radio.openReadingPipe(1, 0xF0F0F0F0E1LL);
	radio.printDetails();
#endif
}

// watchdog interrupt
ISR (WDT_vect) 
{
	wdt_disable();
}

// Sleep the device for 8 seconds...
void sleep8secs()
{
 	MCUSR = 0;     
 	WDTCSR = _BV(WDCE) | _BV(WDE);
	WDTCSR = _BV(WDIE) | _BV(WDP3) | _BV(WDP0);
 	wdt_reset();	      
 	set_sleep_mode(SLEEP_MODE_PWR_DOWN);  
 	sleep_enable();
 	MCUCR = _BV(BODS) | _BV(BODSE);
 	MCUCR = _BV(BODS); 
 	sleep_cpu();
 	sleep_disable();
}


void do_dht(int pin, int sid, int type)
{

 	int chk;
	if(type == 1)
		chk = DHT.read11(pin);
	else if(type == 2)
		chk = DHT.read22(pin);

	Serial.print("D:");
	Serial.print(DEVID);
	Serial.print(" HTS");
	Serial.print(sid);
	Serial.print(": ");
 	switch (chk)
 	{
    	case DHTLIB_OK:
		Serial.print("H:");
		Serial.print(DHT.humidity, 1);
		Serial.print(" T:");
		Serial.println(DHT.temperature, 1);
		break;
	case DHTLIB_ERROR_CHECKSUM: 
      		Serial.println("E:Checksum error");
      		break;
    	case DHTLIB_ERROR_TIMEOUT:
      		Serial.println("E:Time out error");
      		break;
    	default:
      		Serial.println("E:Unknown error");
      	break;
  	}
}

void loop()
{

#if HTS1 != 0
		do_dht(HTS1PIN, 1, HTS1);
#endif
#if HTS2 != 0
		do_dht(HTS2PIN, 2, HTS2);
#endif

	// Ensure serial data is all sent before we go to sleep
	uart_flush();

	// Just the flush is not enough, the serial communications
	// are still corrupt without a delay here.
	delay(500);

	// Go to sleep.
	for(int i=0;i<2;i++)
		sleep8secs();

}



